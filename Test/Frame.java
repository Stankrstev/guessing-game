package Test;

import GuessTheNumber.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Frame extends JFrame {

    private static int playerCoins;
    private Image img;
    private GuessGame guessGame = new GuessGame();
    boolean pressedComplexity = false;
    static File scoreFile = new File("score");


    public void sampleProg() {

        ImageIcon icon = new ImageIcon("Pink_1.png");

        img = icon.getImage();
    }

    public void paint(Graphics g) {
        g.drawImage(img, 0, 0, getSize().width, getSize().height, this);
        g.setColor(Color.black);
        super.paint(g);
    }

    JButton buttonEasy = new JButton("Bot Easy");
    JButton buttonHard = new JButton("Bot Hard");
    JButton guessButton = new JButton("GUESS");


    JLabel label = new JLabel("Hi, welcome to GUESS GAME! so you will be challenged");
    JLabel label2 = new JLabel("by 2 computers, but be carefull both of them wants your profit. ");
    JLabel label3 = new JLabel("First one is Bot Easy~ ~ ~ he is not purfect but at least he will do his best!");
    JLabel label4 = new JLabel("Second one is MaTh ~ ~ ~ he is the best in his class. But if u win against");
    JLabel label5 = new JLabel(" him your profit will be TRIPLE times more!");
    JLabel label6 = new JLabel("Choose your opponent...");
    JLabel label7 = new JLabel("Add your number below.");
    JLabel label8 = new JLabel();
    JLabel label9 = new JLabel();


    JTextField text1 = new JTextField("");
    JPanel background = new JPanel();

    public Frame(String str) throws FileNotFoundException {
        super(str);
        background.setLayout(null);
        background.setBounds(30, 50, 120, 20);

        buttonEasy.setBounds(290, 150, 90, 20);
        buttonHard.setBounds(390, 150, 90, 20);
        guessButton.setBounds(340, 440, 90, 20);

        label.setBounds(230, 35, 400, 20); //ready
        label2.setBounds(220, 45, 400, 20); //ready
        label3.setBounds(210, 65, 400, 20);
        label4.setBounds(210, 85, 420, 20);
        label5.setBounds(210, 95, 400, 20);
        label6.setBounds(320, 125, 400, 20);
        label7.setBounds(320, 230, 400, 20);
        label8.setBounds(320, 300, 400, 140);
        label9.setBounds(320, 140, 400, 140);

        text1.setBounds(290, 250, 190, 30);

        Scanner sc = new Scanner(scoreFile);
        playerCoins = sc.nextInt();

        label9.setText("Player Coins: " + playerCoins);
        buttonEasy.addActionListener(new ActionListener() {
             public void actionPerformed(ActionEvent e) {
                 pressedComplexity = true;
                 buttonEasy.setBackground(Color.BLUE);
                 buttonHard.setBackground(Color.GRAY);
                 guessGame = new GuessGame();
                 Player p = new Player(guessGame);
                 IBot easyBot = new BotEasy();
                 guessGame.initPlayer(p, easyBot);
             }
         }
        );

        buttonHard.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                pressedComplexity = true;
                buttonEasy.setBackground(Color.GRAY);
                buttonHard.setBackground(Color.BLUE);
                guessGame = new GuessGame();
                Player p = new Player(guessGame);
                IBot easyBot = new BotHard();
                guessGame.initPlayer(p, easyBot);
            }
        });

        guessButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(!pressedComplexity) {
                    label8.setText("Please choose bot");
                    return;
                }
                int playerNumber = Integer.parseInt(text1.getText());
                Response response = guessGame.nextGuess(playerNumber);
                label8.setText("<html>" + response.getWholeResponse() + "</html>");
                if(response.isGameEnd()) {
                    buttonEasy.setBackground(Color.WHITE);
                    buttonHard.setBackground(Color.WHITE);
                    pressedComplexity = false;
                    playerCoins += response.getPlayerDiffCoins();
                    label9.setText("Player Coins: " + playerCoins);
                    try {
                        PrintWriter writer = new PrintWriter(scoreFile);
                        writer.println(playerCoins);
                        writer.close();
                    } catch (FileNotFoundException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });

        background.add(buttonEasy);
        background.add(buttonHard);
        background.add(label);
        background.add(label2);
        background.add(label3);
        background.add(label4);
        background.add(label5);
        background.add(label6);
        background.add(label7);
        background.add(label8);
        background.add(label9);
        background.add(guessButton);


        background.add(text1);
        //  background.add(text2);


        getContentPane().add(background);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        pack();
    }

    public Frame() {
    }

    @SuppressWarnings("deprecation")
    public static void main(String[] args) throws IOException {
        Frame frame = new Frame("Guess Game");
        frame.setSize(810, 530);
        frame.show();
        JPanel panel = new JPanel();
        BufferedImage myPicture = ImageIO.read(new File("Pink_1.png"));
        JLabel picLabel = new JLabel(new ImageIcon(myPicture));
        panel.add(picLabel);
        BufferedImage myIcon = ImageIO.read(new File("icon1.png"));
        frame.setIconImage(myIcon);
        frame.setResizable(false);
        frame.setTitle("Guess Game");
        frame.setSize(810, 550);  // snimkata e 800px/450px


    }


}