package GuessTheNumber;

import java.util.Scanner;

public class Player {
    int targetNumber;
    GuessGame gg;

    public Player(GuessGame g) {
        gg = g;
    }

    public void scanner(Response response, int playerNumber) {
        if (1 > playerNumber && playerNumber < 100) {
            System.out.println("My number is.. " + playerNumber + "<br>");
        }
        if (1 > playerNumber || playerNumber > 100) {
            String responseString = "";
            responseString += "ERROR! You can't go out of this range!!! Plase restart the game. " + "<br>";
            response.setPlayerResponse(responseString);
            response.setGameEnd(false);

        }
        targetNumber = gg.targetNumber;
        if (playerNumber < targetNumber) {
            if (playerNumber < gg.min) {
                String responseString = "";
                responseString += "The number is bigger than this number " + "<br>";
                responseString += "between " + gg.min + " to " + gg.max + "<br>";
                response.setPlayerResponse(responseString);
                response.setGameEnd(false);
            }
            if (playerNumber > gg.min) {
                gg.min = playerNumber;
                String responseString = "";
                responseString += "The number is bigger than this number " + "<br>";
                responseString += "between " + gg.min + " to " + gg.max + "<br>";
                response.setPlayerResponse(responseString);
                response.setGameEnd(false);
            }
        }
        if (playerNumber > targetNumber) {

            if (playerNumber < gg.max) {
                gg.max = playerNumber;
                String responseString = "";
                responseString += "The number is less than this number" + "<br>";
                responseString += "between " + gg.min + " to " + gg.max + "<br>";
                response.setPlayerResponse(responseString);
                response.setGameEnd(false);
            } else if (playerNumber > gg.max) {
                String responseString = "";
                responseString += "The number is bigger than this number " + "<br>";
                responseString += "between " + gg.min + " to " + gg.max + "<br>";
                response.setPlayerResponse(responseString);
                response.setGameEnd(false);

            }


        } else if (playerNumber == targetNumber) {
            String responseString = "You Win!" + "<br>";
            response.setPlayerResponse(responseString);
            response.setGameEnd(true);

        }
    }

}
