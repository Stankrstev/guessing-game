package GuessTheNumber;

import java.util.Random;
import java.util.Scanner;

public class GuessGame {
    Player currentPlayer;
    IBot currentBot;
    //    Computer c1;
    // Bot1 b1;
    int counter = 0;
    int coins = 150;
    int min = 0;
    int max = 100;
    static Scanner scan = new Scanner(System.in);
    int targetNumber;
    Random randomGen = new Random();


    public void initPlayer(Player player, IBot bot) {
        targetNumber = randomGen.nextInt(100);
        System.out.println(targetNumber);
        currentPlayer = player;
        currentBot = bot;
    }

    public Response nextGuess(int playerNumber) {
        Response response = new Response();

        boolean p1IsRight = false;
        boolean c1IsRight = false;

        counter++;
        currentPlayer.scanner(response, playerNumber);
        int botNumber = currentBot.guess(min, max, response, targetNumber);


        if (playerNumber == targetNumber) {
            p1IsRight = true;
            scan.close();
        }
        if (botNumber == targetNumber) {
            c1IsRight = true;
        }

        if (p1IsRight) {
            coins = coins / counter;
            String responseString = "";
            responseString += "We got a winner!!!" + "<br>";
            responseString += "Your Prize is: " + coins + " Coins " + "<br>";
            responseString += "Great Job! " + "<br>";
            responseString += "We got a winner!!!" + "<br>";
            responseString += "Count " + counter + "<br>";
            response.setGameResponse(responseString);
            response.setGameEnd(true);
            response.setPlayerDiffCoins(coins);
        }
        if (p1IsRight && c1IsRight) {
            String responseString = "No winner. Player and the bot guessed the number at the same time! " + "<br>";
            response.setGameResponse(responseString);
            response.setGameEnd(true);
        }
        if (c1IsRight) {
            coins = -coins / counter;

            String responseString = "";
            responseString += "You Lose... " + currentBot.getName() + " is the winner! " + "<br>";
            responseString += "You Lost " + coins + " Coins " + "<br>";
            responseString += "Great Job! " + "<br>";
            responseString += "We got a winner!!! " + "<br>";
            responseString += "Count " + counter + "" + "<br>";
            response.setGameResponse(responseString);
            response.setGameEnd(true);
            response.setPlayerDiffCoins(coins);
        }
        return response;


//        }
    }
}
