package GuessTheNumber;

public interface IBot {
    int guess(int min, int max, Response response, int targetNumber);

    String getName();
}
