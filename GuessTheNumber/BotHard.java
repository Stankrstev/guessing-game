package GuessTheNumber;

import java.util.Random;

public class BotHard implements IBot {
    Random randomGen = new Random();

    public int guess(int min, int max, Response response, int targetNumber) {
        int number;
        if (targetNumber != max && targetNumber != max - 1) {
            number = randomGen.nextInt(max - min - 2) + min + 1;
        } else {
            number = randomGen.nextInt(max - min) + min + 1;
        }
        String botResponse = "[Bot hard] My number is.. " + number + "<br>";
        response.setBotResponse(botResponse);

        return number;
    }

    @Override
    public String getName() {
        return "Easy Bot";
    }
}
