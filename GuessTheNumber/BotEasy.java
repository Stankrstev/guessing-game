package GuessTheNumber;

import java.util.Random;

public class BotEasy implements IBot{
	Random randomGen = new Random();

	public int guess(int min, int max, Response response, int targetNumber){
		int number = randomGen.nextInt(max - min) + min + 1;
		String botResponse = "[Bot easy] My number is.. " + number + "<br>";
		response.setBotResponse(botResponse);

		return number;
	}

	@Override
	public String getName() {
		return "Easy Bot";
	}

}
