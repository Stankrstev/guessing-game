package GuessTheNumber;

public class Response {

    boolean isGameEnd;
    String playerResponse;
    String gameResponse;
    String botResponse;
    int playerDiffCoins;
    int botDiffCoins;

    public void setPlayerDiffCoins(int playerDiffCoins) {
        this.playerDiffCoins = playerDiffCoins;
    }

    public void setBotDiffCoins(int botDiffCoins) {
        this.botDiffCoins = botDiffCoins;
    }


    public int getPlayerDiffCoins() {
        return playerDiffCoins;
    }

    public int getBotDiffCoins() {
        return botDiffCoins;
    }

    public void setBotResponse(String botResponse) {
        this.botResponse = botResponse;
    }

    public String getBotResponse() {
        return botResponse;
    }

    public String getPlayerResponse() {
        return playerResponse;
    }

    public String getGameResponse() {
        return gameResponse;
    }

    public void setPlayerResponse(String playerResponse) {
        this.playerResponse = playerResponse;
    }

    public void setGameResponse(String gameResponse) {
        this.gameResponse = gameResponse;
    }

    public void setGameEnd(boolean gameEnd) {
        isGameEnd = gameEnd;
    }

    public boolean isGameEnd() {
        return isGameEnd;
    }

    public String getWholeResponse() {
        String result = "";
        if (playerResponse != null) {
            result += playerResponse;
        }
        if (gameResponse != null) {
            result += gameResponse;
        }
        if (botResponse != null) {
            result += botResponse;
        }
        return result;
    }

}
